# Plugins & Themes API

This is the API reference for plugins and themes. An introduction and quickstart into its use is provided in [the corresponding Contribute guide](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/support/doc/plugins/guide.md).

## Hooks

### Server (only plugins)

[server hooks](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/plugins/server-hook.model.ts ':include :type=code')

### Client
 
[client hooks](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/plugins/client-hook.model.ts ':include :type=code')
[client scopes](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/plugins/plugin-client-scope.type.ts ':include :type=code')


## Server register/unregister (only plugins)

Your library file should export a `register` and `unregister` functions:

[register function](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/server/typings/plugins/plugin-library.model.ts ':include :type=code')

PeerTube provides different helpers to the `register` function:

[register options](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/server/typings/plugins/register-server-option.model.ts ':include :type=code')

### Register hook options

To register hook listeners:

[register settings](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/plugins/register-server-hook.model.ts ':include :type=code')


### Register settings options

To register settings:

[register settings](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/plugins/register-server-setting.model.ts ':include :type=code')


### Settings manager API

You can save/load registered settings:

[settings manager](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/plugins/plugin-settings-manager.model.ts ':include :type=code')


### Storage manager API

To save/load JSON (please don't put too much data in there because we store it in the PeerTube database):

[storage manager](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/plugins/plugin-storage-manager.model.ts ':include :type=code')


### Video categories manager API

[categories manager](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/plugins/plugin-video-category-manager.model.ts ':include :type=code')


### Video languages manager API

[languages manager](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/plugins/plugin-video-language-manager.model.ts ':include :type=code')


### Video licences manager API

[licences manager](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/plugins/plugin-video-licence-manager.model.ts ':include :type=code')


## Client register

Your client script should export a `register` function:

[register function](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/client/src/types/client-script.model.ts ':include :type=code')

PeerTube provides different helpers to the `register` function:

[register options](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/client/src/types/register-client-option.model.ts ':include :type=code')


### Register hook options

To register hook listeners:

[register hook](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/plugins/register-client-hook.model.ts ':include :type=code')
