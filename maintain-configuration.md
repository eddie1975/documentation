# Configurations

## Environment variables

### PT_INITIAL_ROOT_PASSWORD - Initial administrator password

You can set up an initial administrator password with the environment variable `PT_INITIAL_ROOT_PASSWORD`, it must be 6 characters or more.

## Security

Installing PeerTube following the production guide should be secure enough by default. We list here suggestions
to tighten the security of some parts of PeerTube.

### Run ffmpeg with reduced privileges

!> **Warning:** this section is experimental and subject to change. Don't copy commands without adapting them to your situation first.

`ffmpeg` is a project with a [lot of security fixes](https://www.ffmpeg.org/security.html) shipped every
version. Adding firejail, other than adding SELinux or AppArmor support, is a fast and portable way
across all Linux 3.x+ targets to provide some protection from malicious video files uploaded on your instance.
Firejail is a Linux SUID (Set owner User ID up on execution) sandbox with zero dependencies and lots of predefined profiles for common
programs, including ffmpeg. One should always be careful feeding ffmpeg with user-data. Sandboxing
it to, for instance, have no network access, can remove some attack vectors.

Install firejail and firejail-profiles:

```bash
apt install --no-install-recommends firejail firejail-profiles
```

Create the file `/usr/local/bin/jail_ffmpeg`:

```bash
#!/bin/bash
/usr/bin/firejail --writable-var /usr/bin/ffmpeg "$@"
```

Make sure it's executable with `chmod +x /usr/local/bin/jail_ffmpeg`. Repeat for `ffprobe`.

Once your firejail/other wrappers for `ffmpeg` and `ffprobe` are ready, you can make PeerTube use them
via environment variables for the process (here in a systemd unit):

```
Environment=FFMPEG_PATH=/usr/local/bin/jail_ffmpeg
Environment=FFPROBE_PATH=/usr/local/bin/jail_ffprobe
```

Now, `firejail` doesn't support home directories outside of `/home` [yet](https://github.com/netblue30/firejail/issues/2259#issuecomment-438355805), so we need
to relink PeerTube to its own home directory:

- create a symbolic link in `/home` pointing to the user's home directory `/var/www/peertube`
- in `/etc/passwd`, provide this symbolic link as home directory of user peertube

### Systemd Unit with reduced privileges

A systemd unit template is provided at `support/systemd/peertube.service`.

#### `PrivateDevices`

<div class="alert alert-warning" role="alert">
  <strong>Warning:</strong> this won't work on Raspberry Pi. That's
  why we don't enable it by default.
</div>

`PrivateDevices=true` sets up a new `/dev` mount for the Peertube process and
only adds API pseudo devices like `/dev/null`, `/dev/zero`, or `/dev/random`
but not physical devices.

#### `ProtectHome`

`ProtectHome=true` sandboxes Peertube such that the service can not access the
`/home`, `/root`, and `/run/user` folders. If your local Peertube user has its
home folder in one of the restricted places, either change the home directory
of the user or set this option to `false`.


## Cache

Fine-tuning the cache is key to good performance, especially if you have a big instance and/or want to
get the most out of your hardware. The following recommendations may be specific to some situations and
no solution fitting everyone is given.

### Leveraging Nginx Caching

!> **Warning:** this section is experimental and subject to change. Don't copy commands without adapting them to your situation first.

If you are using Nginx, you might want to benefit from its caching capabilities. Of course PeerTube does
application caching itself, but it's nowhere as efficient as a reverse proxy like Nginx when it comes to
static assets (and videos are a static asset, so we have some easy improvement there!). From the [documentation
of Nginx](https://www.nginx.com/blog/nginx-caching-guide/) (which we recommend you read for an in-depth
explanation of what follows):

> A content cache sits in between a client and an “origin server”, and saves copies of all the content it
> sees. If a client requests content that the cache has stored, it returns the content directly without
> contacting the origin server. This improves performance as the content cache is closer to the client,
> and more efficiently uses the application servers because they don’t have to do the work of generating
> pages from scratch each time.

Using the `proxy_cache` directive from Nginx, we can optimize the static route `/static/webseed` that we
already proxy in our configuration.

```nginx
  location /static/webseed {
    # comment this line out:
    # alias /var/www/peertube/storage/videos;

    # add these lines:
    proxy_pass http://localhost:1234;
    proxy_cache peertube_videos;
  }
```

And at the end of the file, outside any server blocks, add this:

```nginx
proxy_cache_path /var/cache/peertube/videos/
    levels=1:2 keys_zone=peertube_videos:10m
    max_size=15g inactive=7d use_temp_path=off;
proxy_cache_valid 200 60m;

server {
  listen localhost:1234;
  location /static/webseed {
    alias /var/www/peertube/storage/videos;
  }
}
```
