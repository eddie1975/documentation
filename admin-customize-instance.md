# Customize your instance

## Install a theme

You can install themes created by the PeerTube community in `Administration > Plugins/Themes > Themes > Search`.

![](/assets/search-themes.png)

Then, you can set your default instance theme in `Administration > Configuration > Basic configuration`.

Your users will be able to override this setting in their preference, so they can choose another theme among those you have installed.

**Learn how to create a theme: https://docs.joinpeertube.org/#/contribute-plugins**

## Customize your instance CSS

If you just want to update some elements of your instance, you can also inject some CSS code configured in  `Administration > Configuration > Advanced configuration`.

It's easy to change the main PeerTube colors using CSS variables using:

```css
body#custom-css {
  --mainColor: #E8252D;
}
```

Check all available variables in [_variables.scss](https://github.com/Chocobozzz/PeerTube/blob/develop/client/src/sass/include/_variables.scss#L68).


## Install a plugin

Administrators can install plugins to change the behaviour of their PeerTube application. Plugins can inject CSS, change the UI and the server logic.

A plugin could for example:
 * Hide some buttons (counters, menu entries...)
 * Reject specific comments or videos (depending on many criterias)
 * Put videos in quarantine
 * Add client analytics (Matomo...)
 * Add a CAPTCHA in the registration form
 * Block some IPs
 * Customize the player
 * ...

You can install plugins created by the PeerTube community in `Administration > Plugins/Themes > Plugins > Search`.

![](/assets/search-plugins.png)

**Learn how to create a plugin: https://docs.joinpeertube.org/#/contribute-plugins**

## Inject JavaScript in client

If you just want to update a particular client behaviour of your instance, you can also inject some JavaScript code configured in  `Administration > Configuration > Advanced configuration`.
