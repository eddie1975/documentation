# Third party applications

## Browser addons

 * [Peertubeify](https://gitlab.com/Ealhad/peertubeify): browser addon to show if a YouTube video exists on PeerTube (by default, it uses [peertube.social](https://peertube.social) to perform the search, but you can change it easily). Available for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/peertubeify/) and on the [Chrome web store](https://chrome.google.com/webstore/detail/peertubeify/gegmikcnabpilimgelhabaledkcikdab)

## Android applications

 * [Thorium](https://github.com/sschueller/peertube-android): an Android PeerTube Client
 * [Fedilab](https://fedilab.app/): Fedilab is a multifunctional Android client to access the distributed Fediverse, consisting of microblogging, photo sharing and video hosting
 * [P2Play](https://gitlab.com/agosto182/p2play) - Android PeerTube client
 * [NewPipe](https://github.com/TeamNewPipe/NewPipe/): NewPipe is a libre lightweight streaming front-end for Android. PeerTube is supported.

## Kodi addons

 * KODI plugin that allows streaming from instances listed publicly, or custom ones you set: https://framagit.org/StCyr/plugin.video.peertube | it only downloads via libtorrent that doesn't support WebTorrent.

## CLI scripts

 * Toot new videos on Mastodon automatically (Python): https://github.com/PhieF/MiscConfig/blob/master/Peertube/peertubetomasto.py
 * Prismedia: scripting video uploads to PeerTube and YouTube at the same time (Python): https://git.lecygnenoir.info/LecygneNoir/prismedia
 * **[FRENCH]** Seed automatically with some bash-fu and WebTorrent Desktop: https://linuxfr.org/users/yolo42/journaux/repliquer-ses-videos-peertube-premiers-pas
 * YouTube2PeerTube: A bot that mirrors YouTube channels to PeerTube channels as videos are released (Python): https://github.com/mister-monster/YouTube2PeerTube

## Web tools

 * [PeerTubatrix](https://booteille.gitlab.io/peertubatrix): A tool to generate uMatrix rules for known PeerTube instances.
 
## Desktop clients

 * [PeerTube Desktop](https://framagit.org/artectrex/peertube-desktop) - GTK desktop client
